﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;


namespace PostillionExtraction
{
    public static class Extention
    {
        public static string GetKeyValue(this string key)
        {
            return ConfigurationManager.AppSettings[key];
        }
    }
}

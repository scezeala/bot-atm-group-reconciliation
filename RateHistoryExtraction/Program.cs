﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RateHistoryExtraction
{
    class Program
    {
        static Logger logger = new Logger();
        static async Task Main(string[] args)
        {

            logger.Info("Extraction Started");
            logger.Info("Loading Configuration");
            var destinationConnection = "destConnectionString".GetKeyValue();

            var destinationTable = "destinationTable".GetKeyValue();
            int timeOut = Convert.ToInt32("timeout".GetKeyValue());
            string resultPath = "resultPath".GetKeyValue();
            bool outcome = false;
            string baseFolder = AppDomain.CurrentDomain.BaseDirectory;
            string queryScript = File.ReadAllText($"{baseFolder}\\Sql\\GetRate.sql");

            InputParam inputParams = new InputParam(args);

            queryScript = queryScript.Replace("{ReconStartDate}", inputParams.ReconStartDate);
            queryScript = queryScript.Replace("{ReconDate}", inputParams.ReconDate);
            queryScript = queryScript.Replace("{Prefix}", inputParams.Prefix);

            try
            {
                Operations ops = new Operations(logger);
                outcome = await ops.BulkCopyToDest2(CancellationToken.None, queryScript, timeOut, destinationTable,
                    inputParams.ConnectionString, destinationConnection);

            }
            catch (Exception ex)
            {
                logger.Info($"An error occourred while inserting into database");
                logger.Error(ex);
            }

            WriteOutput(outcome, resultPath);

        }



        public static void WriteOutput(bool result, string downloadPath)
        {
            if (File.Exists(downloadPath))
            {
                logger.Info("Found an existing file.......");
                logger.Info("Deleting existing file from location.........");
                File.Delete(downloadPath);
                File.WriteAllText(downloadPath, result.ToString());
            }
            else
            {
                File.WriteAllText(downloadPath, result.ToString());
            }
        }
    }
}

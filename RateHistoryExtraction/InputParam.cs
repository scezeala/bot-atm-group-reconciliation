﻿using ConsoleCommon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RateHistoryExtraction
{
    public class InputParam : ParamsObject
    {
        public InputParam(string[] args)
            : base(args)
        {

        }

        [Switch("RSD")]
        public string ReconStartDate { get; set; }

        [Switch("RD")]
        public string ReconDate { get; set; }
        [Switch("PFX")]
        public string Prefix { get; set; }
        [Switch("CNS")]
        public string ConnectionString { get; set; }
    }
}

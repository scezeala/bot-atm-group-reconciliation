﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace GLBalanceExtraction
{
    public class Operations
    {
        string Cluster;
        string Prefix;
        private string connectionString;
        private string oracleConnectionString;
        static Logger logger = new Logger();
        public Operations(string _cluster, string _prefix, string _connectionString, string _oracleConnectionString)
        {
            Cluster = _cluster;
            Prefix = _prefix;
            connectionString = _connectionString;
            oracleConnectionString = _oracleConnectionString;
        }

        public async Task<IEnumerable<TerminalList>> GetAccountTerminals(string script)
        {
            IEnumerable<TerminalList> terminals = null;
            try
            {
                logger.Info($"Retriving Terminal for Cluster {Cluster}");
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    terminals = await connection.QueryAsync<TerminalList>(script, new { Cluster = Cluster });
                }
            }
            catch (Exception ex)
            {
                logger.Info($"An error occurred while getting accounts for Cluster {Cluster}");
                logger.Error(ex);
            }
            logger.Info($"Terminals {terminals.Count()}");
            return terminals;
          
        }

        public async Task<bool> Compute(string previousDate, string currentDate, string previousQuery, string currentQuery, TerminalList terminalList, string updateScript)
        {
            bool output = false;
            try
            {
                //var currentDateBalance = await GetCurrentBalance(terminalList.CURRENCY, previousDate, currentDate, terminalList.ACCT, terminalList.BRANCH_CODE, currentQuery);
                //var previousDateBalance = await GetPreviousBalance(terminalList.CURRENCY, previousDate, terminalList.ACCT, terminalList.BRANCH_CODE, previousQuery);
                var previousDateBalance = await GetPreviousBalance(terminalList.CURRENCY, currentDate, terminalList.ACCT, terminalList.BRANCH_CODE, previousQuery);

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    logger.Info("Updating Table");
                    await connection.ExecuteAsync(updateScript, new
                    {
                        @RunningBalance = previousDateBalance.RunningBalance,
                        @Status = "GLBALANCEEXTRACTED",
                        @Acct = terminalList.ACCT,
                        @BranchCode = terminalList.BRANCH_CODE,
                        @TerminalId = terminalList.TERMINAL_ID,
                        @Cluster = Cluster
                    });
                    output = true;
                }
            }
            catch (Exception ex)
            {
                logger.Info($"An error occurred");
                logger.Error(ex);
            }
            return output;

        }
        public async Task<OutputRecords> GetCurrentBalance(string ccy, string previousDate, string currentDate, string accountNumber,string branchCode, string query)
        {
            var scriptQuery = query.Replace("{CCY}", ccy);
                scriptQuery = scriptQuery.Replace("{StartDate}", previousDate.Replace("-", "").ToLower());
                scriptQuery = scriptQuery.Replace("{EndDate}", currentDate.Replace("-","").ToLower());
                scriptQuery = scriptQuery.Replace("{ACCT}", accountNumber);
                scriptQuery = scriptQuery.Replace("{BRANCH}", branchCode);
                scriptQuery = scriptQuery.Replace("{Prefix}", Prefix);
            logger.Info(scriptQuery);
            OutputRecords model = new OutputRecords();
            try
            {
                using (OracleConnection readConn = new OracleConnection(oracleConnectionString))
                {
                    logger.Info("Opening connection to Database to Get Current Balance");
                    Task openReadConn = readConn.OpenAsync();
                    await Task.WhenAll(openReadConn);
                    DateTime? nodate = null;

                    OracleCommand readCmd = new OracleCommand(scriptQuery, readConn);
                    readCmd.CommandTimeout = Convert.ToInt32("timeout".GetKeyValue());
                    var reader = readCmd.ExecuteReader(CommandBehavior.SequentialAccess);
                    reader.FetchSize = reader.RowSize * 1000;

                    while (reader.Read())
                    {
                        model.AcCcy = reader["AC_CCY"] != DBNull.Value ? (string)reader["AC_CCY"] : "";
                        model.TrnDate = reader["TRN_DT"] != DBNull.Value ? (DateTime?)reader["TRN_DT"] : nodate;
                        model.RunningBalance = reader["RUNNING BALANCE"] != DBNull.Value ? (decimal)reader["RUNNING BALANCE"] : 0.00M;
                        
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Info($"An error occurred while getting Current balance for Account Number {accountNumber}, Branch Code {branchCode} Cluster {Cluster}" );
                logger.Error(ex);
            }
            return model;
        }

        public async Task<OutputRecords> GetPreviousBalance(string ccy, string previousDate, string accountNumber, string branchCode, string query)
        {
            var scriptQuery = query.Replace("{CCY}", ccy);
            scriptQuery = scriptQuery.Replace("{StartDate}", previousDate.Replace("-", "").ToLower());
            scriptQuery = scriptQuery.Replace("{ACCT}", accountNumber);
            scriptQuery = scriptQuery.Replace("{BRANCH}", branchCode);
            scriptQuery = scriptQuery.Replace("{Prefix}", Prefix);
            if(ccy.ToUpper() == "USD")
            {
                scriptQuery = scriptQuery.Replace("{first}", "fcy_amount");
                scriptQuery = scriptQuery.Replace("{second}", "lcy_amount");
            }
            else
            {
                scriptQuery = scriptQuery.Replace("{first}", "lcy_amount");
                scriptQuery = scriptQuery.Replace("{second}", "fcy_amount");
            }

            logger.Info(scriptQuery);
            OutputRecords model = new OutputRecords();
            try
            {
                using (OracleConnection readConn = new OracleConnection(oracleConnectionString))
                {
                    logger.Info("Opening connection to Database to Get Previous Balance");
                    Task openReadConn = readConn.OpenAsync();
                    await Task.WhenAll(openReadConn);
                    DateTime? nodate = null;

                    OracleCommand readCmd = new OracleCommand(scriptQuery, readConn);
                    readCmd.CommandTimeout = Convert.ToInt32("timeout".GetKeyValue());
                    var reader = readCmd.ExecuteReader(CommandBehavior.SequentialAccess);
                    reader.FetchSize = reader.RowSize * 1000;

                    while (reader.Read())
                    {
                        model.AcCcy = reader["AC_CCY"] != DBNull.Value ? (string)reader["AC_CCY"] : "";
                        model.TrnDate = reader["TRN_DT"] != DBNull.Value ? (DateTime?)reader["TRN_DT"] : nodate;
                        model.RunningBalance = reader["RUNNING BALANCE"] != DBNull.Value ? (decimal)reader["RUNNING BALANCE"] : 0.00M;

                    }
                }
            }
            catch (Exception ex)
            {
                logger.Info($"An error occurred while getting Previous balance for Account Number {accountNumber}, Branch Code {branchCode} Cluster {Cluster}");
                logger.Error(ex);
            }
            return model;
        }
    }
}

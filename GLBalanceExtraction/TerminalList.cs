﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GLBalanceExtraction
{
    [Table("ATM_TERMINAL_LIST_GROUP_RECON")]
    public class TerminalList
    {
        public string ACCT { get; set; }
        public string BRANCH_CODE { get; set; }
        public string NEW_NAME { get; set; } 
        public string TERMINAL_ID { get; set; }
        public string CLUSTER { get; set; }
        public string STATUS { get; set; }
        public Nullable<DateTime> RECONDATE { get; set; }
        public string AFFILIATE { get; set; }
        public decimal OPENING_BAL { get; set; }
        public decimal CLOSING_BAL { get; set; }
        public string CURRENCY { get; set; }
    }
}

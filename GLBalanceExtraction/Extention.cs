﻿using System.Configuration;

namespace GLBalanceExtraction
{
    public static class Extention
    {
        public static string GetKeyValue(this string key)
        {
            return ConfigurationManager.AppSettings[key];
        }
    }
}

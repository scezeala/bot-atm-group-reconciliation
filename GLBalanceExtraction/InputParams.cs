﻿using ConsoleCommon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GLBalanceExtraction
{
    public class InputParams : ParamsObject
    {
        public InputParams(string[] args)
         : base(args)
        {

        }

        [Switch("RSD")]
        public string ReconStartDate { get; set; }
        [Switch("RED")]
        public string ReconEndDate { get; set; }
        [Switch("CLT")]
        public string Cluster { get; set; }
       
        [Switch("CNS")]
        public string ConnectionString { get; set; }
        [Switch("PFX")]
        public string Prefix { get; set; }

    }
}

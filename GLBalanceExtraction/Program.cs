﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace GLBalanceExtraction
{
    class Program
    {
        static string baseFolder = $"{AppDomain.CurrentDomain.BaseDirectory}\\SQL\\";
        static Logger logger = new Logger();
        static async Task Main(string[] args)
        {
            string getAccounts = File.ReadAllText($"{baseFolder}SelectReconUser.sql");
            string getCurrentBalance = File.ReadAllText($"{baseFolder}GetCurrentBalance.sql");
            string getPreviousBalance = File.ReadAllText($"{baseFolder}GetPreviousBalance.sql");
            string getUpdateScript = File.ReadAllText($"{baseFolder}UpdateScript.sql");
            string resultPath = "resultPath".GetKeyValue();
            bool outcome = false;
            logger.Info("Extraction Started");
            logger.Info("Loading Configuration");
            var sourceConnection = "sourceConnectionString".GetKeyValue();
            InputParams inputParams = new InputParams(args);
            try
            {
                Operations ops = new Operations(inputParams.Cluster, inputParams.Prefix, sourceConnection, inputParams.ConnectionString);
                var getAccts = await ops.GetAccountTerminals(getAccounts);
                while (getAccts.Any())
                {
                    List<Task> task = new List<Task>();
                    foreach (var account in getAccts)
                    {
                        //await ops.Compute(
                        //     "26oct2020",
                        //    "27oct2020",
                        //     getPreviousBalance,
                        //     getCurrentBalance,
                        //     account,
                        //     getUpdateScript);
                        task.Add(Task.Run(() => ops.Compute(
                            inputParams.ReconStartDate,
                            inputParams.ReconEndDate,
                            getPreviousBalance,
                            getCurrentBalance,
                            account,
                            getUpdateScript)));
                    }
                    Task.WaitAll(task.ToArray());
                    await Task.Delay(2000);
                    getAccts = await ops.GetAccountTerminals(getAccounts);
                }

                outcome = true;



            }
            catch (Exception ex)
            {
                logger.Info($"An error occurred while extracting GL Balance");
                logger.Error(ex);
            }

            WriteOutput(outcome, resultPath);

        }

        public static void WriteOutput(bool result, string downloadPath)
        {
            if (File.Exists(downloadPath))
            {
                logger.Info("Found an existing file.......");
                logger.Info("Deleting existing file from location.........");
                File.Delete(downloadPath);
                File.WriteAllText(downloadPath, result.ToString());
            }
            else
            {
                File.WriteAllText(downloadPath, result.ToString());
            }
        }

    }
}
